(function() {
  'use strict';

  angular
    .module('testPatron')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
