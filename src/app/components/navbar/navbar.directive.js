(function() {
  'use strict';

  angular
    .module('testPatron')
    .directive('patronNavbar', patronNavbar);

  /** @ngInject */
  function patronNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {
      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController(cartService) {
      var vm = this;
      vm.cart = cartService.getCart();

      vm.toggleCart = toggleCart;

      vm.removeArtist = removeArtist;


      function removeArtist(artist) {
        cartService.removeItemFromCart(artist);
      }

      function toggleCart($mdOpenMenu,ev) {
        $mdOpenMenu(ev)

      }
    }
  }

})();
