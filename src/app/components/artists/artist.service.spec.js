/**
 * Created by Bastien Allegret on 18/04/16.
 */

/**
 * Created by Bastien Allegret on 18/04/16.
 */


(function() {
    'use strict';

    describe('cart service',function () {
        var artistService;

        beforeEach(module('testPatron'));
        beforeEach(inject(function (_artistService_) {
            artistService = _artistService_;
        }));

        it('should be registered', function () {
            expect(artistService).not.toEqual(null);
        });


        describe('getArtistList function', function () {
            it('should exist', function () {
                expect(artistService.getArtistList).not.toEqual(null);
            });

            it('should return data', function () {
                var data = artistService.getArtistList();

                expect(data).toEqual(jasmine.any(Array));
            });
        });

    })
})();
