/**
 * Created by Bastien Allegret on 18/04/16.
 */

(function () {
    'use strict';

    angular
        .module('testPatron')
        .service('artistService',artistService);

    /** @ngInject */
    function artistService(imageAvailability) {
        var data = [
            {
                name: "Backflip",
                price: "100",
                availability: imageAvailability.AVAILABLE,
                image: "1.png"
            }, {
                name: "String Fight",
                price: "150",
                availability: imageAvailability.SOLD_OUT,
                image: "2.jpg"
            }, {
                name: "Our own Trumpet",
                price: "175",
                availability: imageAvailability.AVAILABLE,
                image: "3.png"
            }, {
                name: "The Rockstar",
                price: "475",
                availability: imageAvailability.SOLD_OUT,
                image: "4.png"
            }, {
                name: "Mic out",
                price: "125",
                availability: imageAvailability.AVAILABLE,
                image: "5.png"
            }

        ];

        this.getArtistList = getArtistList;

        // retrieve the data
        function getArtistList() {
            return data;
        }
    }
})();