/**
 * Created by Bastien Allegret on 18/04/16.
 */

(function () {
    'use strict';

    angular
        .module('testPatron')
        .service('cartService', cartService);

    /** @ngInject */
    function cartService() {
        // Cart data
        var cart = [];

        /// TODO: Persistence :retrieve cart from localStorage


        this.getCart = getCart;
        this.addItemToCart = addItemToCart;
        this.removeItemFromCart = removeItemFromCart;
        this.clearCart = clearCart;

        // retriev the cart
        function getCart() {
            return cart;
        }

        // Add an item to the current cart
        // return true if operation successful, false otherwise
        function addItemToCart(artist) {
            if (typeof artist === "undefined") {
                return false;
            } else if (cart.indexOf(artist) !== -1) {
                return false;
            } else {
                ///TODO: persistence

                cart.push(artist);
                return true;
            }
        }

        // Remove the given item from the current cart
        // return true if operation successful, false otherwise
        function removeItemFromCart(artist) {
            if (typeof artist === "undefined") {
                return false;
            } else if (cart.indexOf(artist) === -1) {
                return false;
            } else {
                ///TODO: persistence

                var index = cart.indexOf(artist);
                cart.splice(index, 1);
                return true;
            }
        }


        // clear the cart
        function clearCart() {
            cart.slice(0, cart.length);
        }

    }

})();