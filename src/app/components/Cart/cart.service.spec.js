/**
 * Created by Bastien Allegret on 18/04/16.
 */


(function() {
    'use strict';

    describe('cart service',function () {
        var cartService;
        var imageAvailability;

        beforeEach(module('testPatron'));
        beforeEach(inject(function (_cartService_,_imageAvailability_) {
            cartService = _cartService_;
            imageAvailability = _imageAvailability_;
        }));

        it('should be registered', function () {
            expect(cartService).not.toEqual(null);
        });


        describe('getCart function', function () {
            it('should exist', function () {
                expect(cartService.getCart).not.toEqual(null);
            });

            it('should return data', function () {
                var data = cartService.getCart();

                expect(data).toEqual(jasmine.any(Array));
            });

            ///TODO: Test Persistence
        });


        describe('AddItemToCart function', function () {
            beforeEach(function () {
                cartService.clearCart();
            });


            it('should exist', function () {
                expect(cartService.addItemToCart).not.toEqual(null);
            });

            it('should return false if item undefined', function () {
                var item;
                var result = cartService.addItemToCart(item);

                expect(result).toEqual(false);
            });


            it('should return true and add item', function () {
                var item =  {
                    name: "backflip",
                    price: "100",
                    availability: imageAvailability.AVAILABLE,
                    image: "assests/images/1.png"
                };

                // add the first


                var result = cartService.addItemToCart(item);
                var cart = cartService.getCart();

                expect(result).toEqual(true);
                expect(cart).toEqual(jasmine.arrayContaining([item]));
            });

            it('should return false and not add item if already in cart', function () {
                var item =  {
                    name: "backflip",
                    price: "100",
                    availability: imageAvailability.AVAILABLE,
                    image: "assests/images/1.png"
                };

                // add the first
                cartService.addItemToCart(item);

                var result = cartService.addItemToCart(item);

                expect(result).toEqual(false)
            });

            ///TODO: Test Persistence

        });


        describe('removeItemFromCart function', function () {
            var item;
            beforeEach(function () {
                cartService.clearCart();
                item = {
                    name: "backflip",
                    price: "100",
                    availability: imageAvailability.AVAILABLE,
                    image: "assests/images/1.png"
                };
            });


            it('should exist', function () {
                expect(cartService.removeItemFromCart).not.toEqual(null);
            });


            ///TODO: Test Persistence
            it('should return false if item undefined', function () {
                var nullItem;
                var result = cartService.removeItemFromCart(nullItem);

                expect(result).toEqual(false);
            });


            it('should return true and remove item', function () {
                cartService.addItemToCart(item);

                var result = cartService.removeItemFromCart(item);
                var cart = cartService.getCart();

                expect(result).toEqual(true);
                expect(cart).not.toEqual(jasmine.arrayContaining([item]));
            });

            it('should return false and not add item if already in cart', function () {
                // remove the item
                cartService.removeItemFromCart(item);

                //try ot remove it again
                var result = cartService.removeItemFromCart(item);

                expect(result).toEqual(false)
            });
        });
    })
})();
