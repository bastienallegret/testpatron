(function() {
  'use strict';

  angular
    .module('testPatron', ['ngAnimate', 'ngCookies', 'ngMessages', 'ngAria', 'ui.router', 'ngMaterial', 'toastr']);

})();
