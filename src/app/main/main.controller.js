(function() {
  'use strict';

  angular
    .module('testPatron')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, toastr,cartService,imageAvailability,artistService) {
    var vm = this;


    vm.addToCart = function (artist) {
      cartService.addItemToCart(artist)
    };

    vm.availability = imageAvailability;

    vm.artistInCart = function (artist) {
      return cartService.getCart().indexOf(artist) !== -1;
    };


    activate();

    function activate() {
      vm.artists = artistService.getArtistList();
    }
  }
})();
