/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('testPatron')
    .constant('imageAvailability', {AVAILABLE: 1, SOLD_OUT:0})
    .constant('moment', moment);

})();
