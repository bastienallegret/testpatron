# README #

Patron Test Application 

### What is this repository for? ###

### How do I get set up? ###

The project was initiated with a yeoman [generator](https://github.com/Swiip/generator-gulp-angular) 

To start :

install packages
 
    npm install
    bower install

finally build the project 

    gulp

### What's in there ###

* Full functionality (except Persistence, see below) 
    * Add artist to cart
    * Remove artist from cart
    * Availability of artist allow/prevents adding
* Angular Material design
* Layout breaking at requested device break (with deltas on breakpoints, see below)
* Angular service tests


### What's missing ###

* I haven't had the time to implement cart data persistent, I was thinking of using localStorage to achieve it
* The total amount of cart item is not on there, I forgot to put it (nothing a forEach can't fix)
* I overestimated my knowledge of angular material, and so I'm a little off on layouts margins and the md-menu item
* breakpoints are still in their vendor default for angular material (600px, 960px, 1280px, 1920px)